#!/usr/bin/python3

"""

Patrick Thomas

Takes analog input from an ADS1115 breakout board connected to a Raspberry Pi 3 and
displays it on a web page via REMi.

"""

import threading
import time

import Adafruit_ADS1x15 as ADC
import remi

# contants
GAIN = 1  			# voltage ranges from -4.096 to 4.096
MAX_V = 4.096		# the max voltage that the ADS1115 can read
WINDOW = 20			# how many past measurements are stored and used in
					# calculating the average
DELAY = 2.0/WINDOW	# how often the inputs are read
SHORT_INT_LEN = pow(2, 16 - 1) - 1	# for adjusting the ADS1115 inputs
ADDRESS = "0.0.0.0"	# address of the server
PORT = 8081			# port of the server
N_CHANNELS = 4		# number of inputs that the ADS1115 has




class AnalogDataReader():
	"""
	An interface to the ADS1115 to read both the raw input as well as 
	the voltage of the input.
	"""
	def __init__(self, adc):
		self.values = {i:0 for i in range(0, 4)}
		self.voltages = {i:0.0 for i in range(0, 4)}
		self.adc = adc

	def read_values(self):
		"""
		Read the values of the inputs from the ADS1115 and calculate
		voltages from those inputs.
		"""
		for i in self.values.keys():
			self.values[i] = self.adc.read_adc(i, gain=GAIN)
			self.voltages[i] = to_voltage(self.values[i])
	
	def get_value(self, key):
		"""
		Retrieve a stored raw input.
		"""
		return self.values[key]

	def get_voltage(self, key):
		"""
		Retrieve a stored voltage.
		"""
		return self.voltages[key]



class AnalogDataReaderRunningAvg():
	"""
	An interface to the ADS1115 to read both the raw input as well as 
	the voltage of the input. This version computes a running average
	for the past n frames.
	"""
	def __init__(self, adc, frames=5):
		self.frames = 5;
		self.head = 0;

		self.values = [{i:0 for i in range(0, 4)} for j in range(0, frames)]
		self.adc = adc

	def read_values(self):
		"""
		Read the values of the inputs from the ADS1115 and calculate
		voltages from those inputs.
		"""
		for i in self.values[self.head].keys():
			self.values[self.head][i] = self.adc.read_adc(i, gain=GAIN)
			self.head += 1
			self.head %= self.frames
	
	def get_value(self, key):
		"""
		Retrieve a stored raw input.
		"""
		sum = 0
		for i in range(0, self.frames):
			sum += self.values[i][key]
		return int(sum/self.frames)

	def get_voltage(self, key):
		"""
		Retrieve a stored voltage.
		"""
		return to_voltage(self.get_value(key))


class TableUpdateThread(threading.Thread):
	"""
	A thread that updates the table's values every DELAY seconds.
	"""
	def __init__(self, data, table):
		threading.Thread.__init__(self)
		self.data = data
		self.table = table

	def run(self):
		while(True):
			for y in range(1, self.table.row_count()):
				# set table values for raw inputs
				text = "{:>16}".format(self.data.get_value(y-1))
				self.table.item_at(y, 1).set_text(text)
				
				# set for voltages
				text = "{:>16.3f}V".format(self.data.get_voltage(y-1))
				self.table.item_at(y, 2).set_text(text)

			self.data.read_values() # get new data from the sensors
			time.sleep(DELAY)



class PatrickApp(remi.App):
	def __init__(self, *args):
		super(PatrickApp, self).__init__(*args)

	def main(self):
		data = AnalogDataReaderRunningAvg(ADC.ADS1115(), WINDOW)
		table = remi.gui.TableWidget(
				5, 	# number of columns
				3, 	# number of rows
				True, 	# first row is header?
				False, 	# is the table editable?
				width=300, height=400
		)

		# initialize values of table
		table.item_at(0, 0).set_text("Channel")
		table.item_at(0, 1).set_text("Raw output")
		table.item_at(0, 2).set_text("Voltage")
		for y in range(1, table.row_count()):
			table.item_at(y, 0).set_text(str(y))

		# start the update thread
		thread = TableUpdateThread(data, table)
		thread.start()

		return table



def to_voltage(adc_input):
	"""
	Convert an output from the ADS1115 to a voltage using the 
	maximum voltage for the selected gain as well as the maximum
	size of the number (16 bits -> short int)
	"""
	return MAX_V * adc_input / SHORT_INT_LEN


if __name__ == "__main__":
    # main loop
    remi.start(
        PatrickApp,
        address=ADDRESS,
        port=PORT,
        multiple_instance=False,
        enable_file_cache=False,
        update_interval=DELAY,
        start_browser=False)
