# Raspberry Pi ADS1115 Web Display

![Screen shot of interface](http://i66.tinypic.com/eknxqx.jpg)

This project extensively uses:

-	[The Adafruit ADS1x15 library](https://github.com/adafruit/Adafruit_ADS1X15)
-	[REMi](https://github.com/dddomodossola/remi)


